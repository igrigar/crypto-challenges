#!/usr/bin/env python
# -*- coding: utf8 -*-
import sys
import operator
import binascii
from collections import Counter

print("The cryptopals crypto challenges")
print("Challenge 6: Break repeating-key XOR")

# Ascii printable characters lower and higher value.
lower_bound = 32
upper_bound = 126

min_key_len = 2
max_key_len = 40

key = ""

# From http://www.data-compression.com/english.html
letter_frequency = { 'a': 0.0651738, 'b': 0.0124248, 'c': 0.0217339, 'd': 0.0349835, 'e': 0.1041442, 'f': 0.0197881, 'g': 0.0158610, 'h': 0.0492888, 'i': 0.0558094, 'j': 0.0009033, 'k': 0.0050529, 'l': 0.0331490, 'm': 0.0202124, 'n': 0.0564513, 'o': 0.0596302, 'p': 0.0137645, 'q': 0.0008606, 'r': 0.0497563, 's': 0.0515760, 't': 0.0729357, 'u': 0.0225134, 'v': 0.0082903, 'w': 0.0171272, 'x': 0.0013692, 'y': 0.0145984, 'z': 0.0007836, ' ': 0.1918182}

def hamming_distance_bin(s1, s2):
    hamming_distance = 0

    assert len(s1) == len(s2)

    for i in range(0, len(s1)):
        x = s1[i] ^ s2[i]
        s = 0
        while (x > 0):
            s += x & 1
            x >>= 1
        hamming_distance += s
    return hamming_distance

with open("Challenge_6.txt") as f:
    input_bin = b"".join([binascii.a2b_base64(line.rstrip()) for line in f])


# We calculate the normalized hamming distance of key length blocks.
norm_hamming_distance = [sum([hamming_distance_bin(input_bin[i*key_len:(i+1)*key_len], input_bin[(i+1)*key_len:(i+2)*key_len]) for i in range(0, 10)])/(10*key_len) for key_len in range(min_key_len, max_key_len)]

# We get the key length
key_len = norm_hamming_distance.index(min(norm_hamming_distance)) + 2

# We pad the block to have even length blocks
input_bin += (key_len - (len(input_bin)%key_len)) * b"\x00"

# We divide the ciphertext in key length blocks
blocks = [input_bin[i:i+key_len] for i in range(0, len(input_bin), key_len)]

# We transpose the blocks
blocks_t =["".join([chr(blocks[i][j]) for i in range(len(blocks))]) for j in range(len(blocks[0]))]

for i in range(0, key_len):
    cipher_text = blocks_t[i]
    encoded_mfb = max(Counter(cipher_text), key=Counter(cipher_text).get)
    key_part = ("", 0)

    for j in range(lower_bound, upper_bound):
        decoded_mfb = j
        k = ord(encoded_mfb) ^ decoded_mfb
        decoded_msg = ''.join([chr(ord(b) ^ k) for b in cipher_text])
        score = sum([letter_frequency[c] for c in Counter(decoded_msg) if c in letter_frequency])

        if score > key_part[1]: key_part = (chr(k), score)
    key += key_part[0]

plain_text = "".join(["".join([chr(b[i] ^ ord(key[i%len(key)])) for i in range(0, key_len)]) for b in blocks])
print(plain_text)
