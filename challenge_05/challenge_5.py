#!/usr/bin/env python
# -*- coding: utf8 -*-
import sys
import operator
import binascii
from collections import Counter

print("The cryptopals crypto challenges")
print("Challenge 5: Implement repeating-key XOR")

plain_text = """Burning 'em, if you ain't quick and nimble
I go crazy when I hear a cymbal"""
#plain_text = raw_input("What text would you like to cipher? ")
key = input("Insert the encription password: ")

cipher_text = "".join([str(hex(ord(plain_text[i]) ^ ord(key[i%len(key)]))[2:]) for i in range(0, len(plain_text))])

print(cipher_text)
