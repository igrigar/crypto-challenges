#!/usr/bin/env python
# -*- coding: utf8 -*-
import sys
import operator
import binascii
from collections import Counter

print("The cryptopals crypto challenges")
print("Challenge 3: Single-byte XOR cipher")

# From http://www.data-compression.com/english.html
letter_frequency = { 'a': 0.0651738, 'b': 0.0124248, 'c': 0.0217339, 'd': 0.0349835, 'e': 0.1041442, 'f': 0.0197881, 'g': 0.0158610, 'h': 0.0492888, 'i': 0.0558094, 'j': 0.0009033, 'k': 0.0050529, 'l': 0.0331490, 'm': 0.0202124, 'n': 0.0564513, 'o': 0.0596302, 'p': 0.0137645, 'q': 0.0008606, 'r': 0.0497563, 's': 0.0515760, 't': 0.0729357, 'u': 0.0225134, 'v': 0.0082903, 'w': 0.0171272, 'x': 0.0013692, 'y': 0.0145984, 'z': 0.0007836, ' ': 0.1918182 
}

try:
    input_bin = binascii.unhexlify(input("Insert the first string you want to decode: "))
except Exception as e:
    print("It seems that you haven't entered an hex input")
    sys.exit()

# Encoded most frequent byte.
encoded_mfb = max(Counter(input_bin), key=Counter(input_bin).get)

# Decoded most frequent byte.
decoded_mfb = ord(max(letter_frequency, key=letter_frequency.get))

# Get the key
key = encoded_mfb ^ decoded_mfb

# Decode the message
decoded_msg = "".join([chr(b ^ key) for b in input_bin])

print(decoded_msg)
