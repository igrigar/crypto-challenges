#!/usr/bin/env python
# -*- coding: utf8 -*-
import sys
import operator
import binascii
from collections import Counter

print("The cryptopals crypto challenges")
print("Challenge 8: Detect AES in ECB mode")

block_size = 16

with open("Challenge_8.txt") as f:
    input_bin = [binascii.a2b_base64(line.rstrip()) for line in f]

for i, line in enumerate(input_bin):
    blocks = [line[i:i+block_size] for i in range(0, len(line), block_size)]
    if len(blocks) != len(set(blocks)):
        print(i)
