#!/usr/bin/env python
# -*- coding: utf8 -*-
import sys
import binascii

print("The cryptopals crypto challenges")
print("Challenge 2: Fixed XOR")

try:
    hex_input_1 = int(input("Insert the first string you want to XOR: "), 16)
    hex_input_2 = int(input("Insert the second string you want to XOR: "), 16)
except Exception as e:
    print("It seems that you haven't entered an hex input")
    sys.exit()

output = hex(hex_input_1 ^ hex_input_2)
print(output)
