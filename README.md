# Crypto Challenges

Solution to the cryptographic challenges defined by Thomas Ptacek, Sean Devlin, Alex Balducci and Marcin Wielgoszewski in The Cryptopals Crypto Challenges (https://cryptopals.com/)

All the solutions are written in python3.
