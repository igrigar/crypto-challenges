#!/usr/bin/env python
# -*- coding: utf8 -*-
import sys
import binascii

print("The cryptopals crypto challenges")
print("Challenge 1: Convert hex to base64")

hex_input_raw = input("Insert the hex string you want to convert: ")

try:
    print(binascii.b2a_base64(binascii.unhexlify(hex_input_raw)).decode('ascii'))
except Exception as e:
    print("It seems that you haven't entered an hex input")
    sys.exit()
