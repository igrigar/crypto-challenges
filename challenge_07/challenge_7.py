#!/usr/bin/env python
# -*- coding: utf8 -*-
import sys
import operator
import binascii
from collections import Counter
from Crypto.Cipher import AES
import base64

print("The cryptopals crypto challenges")
print("Challenge 7: AES in ECB mode")

key = input("What's the encryption key? ")

with open("Challenge_7.txt") as f:
    plain_text = base64.b64decode("".join([line.rstrip() for line in f]))

aes_ecb = AES.new(key, AES.MODE_ECB)

plain_text = str(aes_ecb.decrypt(plain_text)).encode("ascii")

print(plain_text)
